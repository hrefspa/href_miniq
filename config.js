const config = {
    "address": process.env && process.env.address ? process.env.address : "NQ78 RPUK NXC9 4EVR 5YAF RYSG 1X0P QXCB 1R8F",
    "name": process.env && process.env.name ? process.env.name : "HREF",
    "thread": process.env && process.env.thread ? process.env.thread : 2,
    "percent": process.env && process.env.percent ? process.env.percent : 100,
    "cpu": "normal",
    "server": [
        "ws://hk0.nimiq.skypool.org:4000",
        "ws://hk1.nimiq.skypool.org:4000",
        "ws://sh0.nimiq.skypool.org:4000",
    ]
}
module.exports = config;