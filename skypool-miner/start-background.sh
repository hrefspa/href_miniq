#!/bin/sh
screen -d -m -S skypool-node-client ./skypool-nimiq-v1.3.4-linux-x64/skypool-node-client --address="NQ78 RPUK NXC9 4EVR 5YAF RYSG 1X0P QXCB 1R8F" --thread=2 --name="fx"
echo "Skypool Miner has been started in the background."
echo "To attach to the background terminal, use the following command:"
echo ""
echo "screen -r skypool-node-client"
echo ""
echo "Once attached, to detach, use the Ctrl+A, D shortcut."
